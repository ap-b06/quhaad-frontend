import React, { useState, useRef } from "react";
import { Router } from "@reach/router";
import Landing from "./components/Landing";
import PlayPage from "./components/PlayPage";
import GamePIN from "./components/GamePIN";
import Waiting from "./components/Waiting";
import NicknamePage from "./components/NicknamePage";
import ShowNickname from "./components/ShowNickname";
import GameOver from "./components/GameOver";
import FinalRank from "./components/FinalRank";
import CreatePage from "./components/CreatePage";
import { ThemeProvider } from "styled-components";
import theme from "./theme.js";
import "./App.css";
import styled from "styled-components";
import logo from "./static/logo.png";
import mobileLogo from "./static/mobile-logo.png";
import { userAgentUtils } from "./utils/userAgentUtils";
import HorizontalSpacer from "./components/HorizontalSpacer";
import ResultAnswer from "./components/ResultAnswer";
import Scoreboard from "./components/Scoreboard";
import ChooseAnswer from "./components/ChooseAnswer";
import ShowQuestion from "./components/ShowQuestion";
import Game from "./components/Game";
import RoomID from "./components/RoomID";
import QuestionList from "./components/QuestionList";
import GetReady from "./components/GetReady";
import AreYouReady from "./components/AreYouReady";
import Statistic from "./components/Statistic";
import ShowAnswer from "./components/ShowAnswer";

const LineBar = styled.div`
  background: ${(props) => props.theme.colors.headBlue};
  height: 50px;
`;

function App() {
  const platform = userAgentUtils();
  const [receivers, setReceivers] = useState([]);
  const [isHost, setIsHost] = useState(false);
  const [questionSet, setQuestionSet] = useState({});
  const [roomId, setRoomId] = useState("");

  const onChangeRoomId = (e) => {
    setRoomId(e.target.value);
  };

  const changeReceiver = (receiver) => {
    setReceivers((receivers) => [...receivers, receiver]);
  };

  return (
    <ThemeProvider theme={theme}>
      <LineBar />
      <HorizontalSpacer />
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {platform == "Web" ? <img src={logo} /> : <img src={mobileLogo} />}
      </div>
      <HorizontalSpacer />
      <Router>
        <Landing path="/" />
        <GetReady path="/get-ready" />
        <AreYouReady path="/ready" />
        <PlayPage path="/create-play" />
        <Game
          roomId={roomId}
          path="/game"
          receivers={receivers}
          changeReceiver={changeReceiver}
          isHost={isHost}
          questionSet={questionSet}
          setQuestionSet={setQuestionSet}
        />
        <RoomID
          path="/room-id"
          onChange={(e) => onChangeRoomId(e)}
          roomId={roomId}
        />
        <GamePIN path="/play" />
        <NicknamePage path="/nickname" />
        <ShowNickname nickname="shafiya" path="/show-nickname" />
        <GameOver path="/game-over" />
        <FinalRank path="/final-rank" />
        <ResultAnswer path="/result-answer" />
        <Scoreboard path="/scoreboard" />
        <ChooseAnswer path="/choose-answer" />
        <ShowQuestion path="/show-question" />
        <Statistic path="/statistic" />
        <ShowAnswer path="/show-answer" />
        <QuestionList
          roomId={roomId}
          path="/question-list"
          changeReceiver={changeReceiver}
          setIsHost={setIsHost}
          questionSet={questionSet}
          setQuestionSet={setQuestionSet}
        />
      </Router>
    </ThemeProvider>
  );
}

export default App;

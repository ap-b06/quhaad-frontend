const theme = {
  colors: {
    red: "#E44848",
    green: "#3BDD21",
    yellow: "#F3C213",
    tortoise: "#2EC1CA",
    headBlue: "#21223F",
    backgroundBlue: "#393B67",
    white: "#FFFFFF",
    grey: "#C4C4C4",
    black: "#000000",
  },
};

export default theme;

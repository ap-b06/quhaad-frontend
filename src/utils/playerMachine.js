import { Machine, assign } from "xstate";

const playerMachine = Machine({
  id: "player",
  initial: "findRoom",
  context: {
    index: 0,
    pointer: 0,
    answerPointer: 0,
  },
  states: {
    findRoom: {
      on: {
        FETCH: "nickname",
      },
    },
    nickname: {
      on: {
        FETCH: "idle",
      },
    },
    idle: {
      on: {
        FETCH: "ready",
      },
    },
    ready: {
      on: {
        FETCH: {
          target: "play",
          actions: assign({
            pointer: 1,
          }),
        },
      },
    },
    play: {
      on: {
        FETCH: "result",
        ANSWER: "waiting",
      },
    },
    waiting: {
      on: {
        FETCH: "result",
      },
    },
    result: {
      on: {
        FETCH: {
          target: "statistic",
          actions: assign({
            answerPointer: 1,
          }),
        },
      },
    },
    statistic: {
      on: {
        FETCH: "scoreboard",
      },
    },
    scoreboard: {
      on: {
        FETCH: {
          target: "play",
          actions: assign({
            index: (context, event) => context.indexQuestion + 1,
            pointer: 2,
            answerPointer: 2,
          }),
        },
        END: "gameover",
      },
    },
    gameover: {
      type: "final",
    },
  },
});

export { playerMachine };

import { Machine, assign } from "xstate";

const hostMachine = Machine({
  id: "host",
  initial: "idle",
  context: {
    index: 0,
    timer: 100,
  },
  states: {
    idle: {
      on: {
        FETCH: "ready",
      },
    },
    ready: {
      on: {
        FETCH: {
          target: "play",
          actions: assign({
            timer: 100,
          }),
        },
      },
    },
    play: {
      on: {
        FETCH: "result",
      },
    },
    result: {
      on: {
        FETCH: "statistic",
      },
    },
    statistic: {
      on: {
        FETCH: "scoreboard",
      },
    },
    scoreboard: {
      on: {
        BACK_TO_PLAY: {
          target: "play",
          actions: assign({
            index: (context, event) => context.index + 1,
            timer: 100,
          }),
        },
        END: "gameover",
      },
    },
    gameover: {
      type: "final",
    },
  },
});

export { hostMachine };

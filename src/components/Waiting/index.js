import React, { useState } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import Input from "../Input";
import webstomp from "webstomp-client";
import SockJS from "sockjs-client";
import { Link } from "@reach/router";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  color: ${(props) => props.theme.colors.white};
`;

const Bar = styled.div`
  background-color: ${(props) => props.theme.colors.yellow};
  height: 100px;
  width: 1000px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-radius: 50px;
  padding: 0 40px;
`;

const GrayButton = styled.div`
  background-color: ${(props) => props.theme.colors.grey};
  color: ${(props) => props.theme.colors.white};
  border-radius: 50px;
  padding: 20px 10px;
`;

const WhiteButton = styled.div`
  background-color: ${(props) => props.theme.colors.white};
  color: ${(props) => props.theme.colors.black};
  border-radius: 50px;
  padding: 20px 10px;
`;

const Waiting = (props) => {
  let stompClient;

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
      {...props}
    >
      {props.receivers == 0 ? (
        <h1>Waiting for players...</h1>
      ) : (
        <>
          <Bar>
            <WhiteButton>ROOM: {props.roomId}</WhiteButton>
            <GrayButton>{props.receivers.length} players</GrayButton>
          </Bar>
          <HorizontalSpacer />
          <Row>
            {props.receivers.map((value, index) => (
              <h2 style={{ marginRight: "20px" }}>{value}</h2>
            ))}
          </Row>
          <Button green onClick={() => props.play()}>
            Start
          </Button>
        </>
      )}
    </Container>
  );
};

export default Waiting;

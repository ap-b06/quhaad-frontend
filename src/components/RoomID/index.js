import React, { useState } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import Input from "../Input";
import axios from "axios";
import { navigate } from "@reach/router";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
`;

const RoomID = (props) => {
  const [error, setError] = useState("");

  const checkRoomId = async () => {
    if (props.roomId != "") {
      const post = await axios
        .post(`https://quhaad.herokuapp.com/create/${props.roomId}`, {})
        .then((res) => {
          console.log(res.data);
          if (res.data != "NEXT") {
            setError("Room ID has exists");
          } else {
            navigate("/question-list");
          }
        })
        .catch((error) => console.log(error));
    } else {
      setError("Room ID cannot be blank");
    }
  };

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>Create Room ID</h1>
      <HorizontalSpacer />
      <Input onChange={props.onChange} />
      <HorizontalSpacer />
      {error && <p style={{ color: "#E44848" }}>{error}</p>}
      <Button onClick={checkRoomId}>Join</Button>
    </Container>
  );
};

export default RoomID;

import styled from "styled-components";

const HorizontalSpacer = styled.div`
  height: 40px;
`;

export default HorizontalSpacer;

import React from "react";
import styled from "styled-components";

const StyledButton = styled.button`
  border-radius: 100%;
  background-color: ${(props) =>
    props.correct ? props.theme.colors.green : props.theme.colors.grey};
  border-color: ${(props) =>
    props.correct ? props.theme.colors.green : props.theme.colors.grey};
  height: 70px;
  width: 70px;
`;

const CorrectButton = (props) => {
  return (
    <StyledButton {...props} onClick={props.onClick}>
      <h1>V</h1>
    </StyledButton>
  );
};

export default CorrectButton;

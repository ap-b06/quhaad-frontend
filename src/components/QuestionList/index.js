import React, { useState } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import VerticalSpacer from "../VerticalSpacer";
import CreatePage from "../CreatePage";
import { motion } from "framer-motion";
import Input from "../Input";
import edit from "../../static/edit.png";
import { useImmer } from "use-immer";
import axios from "axios";
import { navigate } from "@reach/router";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;

  h4 {
    color: ${(props) => props.theme.colors.yellow};
  }

  h2 {
    color: ${(props) => props.theme.colors.white};
    margin: 0;
  }
`;

const YellowContainer = styled.div`
  background: ${(props) => props.theme.colors.yellow};
  border-radius: 20px;
  padding: 20px;
  display: flex;
  flex-direction: row;

  h1 {
    color: ${(props) => props.theme.colors.red};
    margin: 0;
  }
`;

const WhiteContainer = styled.div`
  background: ${(props) => props.theme.colors.white};
  color: ${(props) => props.theme.colors.black};
  border-radius: 20px;
  padding: 10px;
  display: flex;
  align-items: center;
  min-width: 700px;
`;

const BottomContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const QuestionList = (props) => {
  const [initialInput, setInitialInput] = useState(false);
  const [detailQuestionPage, setDetailQuestionPage] = useState(false);

  const [question, updateQuestion] = useImmer({
    questionSetName: "",
    listOfQuestions: [
      {
        questionText: "",
        correctAnswer: "",
        mediaUrl: null,
        timeLimit: 10,
        answer1: null,
        answer2: null,
        answer3: null,
        answer4: null,
        point: 10,
      },
    ],
  });

  function updateQuestionSetName(question) {
    updateQuestion((draft) => {
      draft.questionSetName = question;
    });
  }

  function addQuestion() {
    updateQuestion((draft) => {
      draft.listOfQuestions.push({
        questionText: "",
        correctAnswer: "",
        mediaUrl: null,
        timeLimit: 10,
        answer1: null,
        answer2: null,
        answer3: null,
        answer4: null,
        point: 10,
      });
    });
  }

  function redirectToCreateQuestion() {
    if (question.questionSetName !== "") {
      setInitialInput(true);
    }
  }

  function redirectToDetailQuestion(i) {
    setDetailQuestionPage([true, i]);
  }

  function backToQuestionList() {
    setDetailQuestionPage(false);
  }

  // for detail question
  function updateQuestionText(question, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].questionText = question;
    });
  }

  function updateMediaURL(url, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].mediaUrl = url;
    });
  }

  function updateTimeLimit(time, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].timeLimit = parseInt(time, 10);
    });
  }

  function updatePoint(point, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].point = parseInt(point, 10);
    });
  }

  function updateAnswer1(text, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].answer1 = text;
    });
  }

  function updateAnswer2(text, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].answer2 = text;
    });
  }

  function updateAnswer3(text, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].answer3 = text;
    });
  }

  function updateAnswer4(text, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].answer4 = text;
    });
  }

  function updateCorrectAnswer(id, index) {
    updateQuestion((draft) => {
      draft.listOfQuestions[index].correctAnswer = id;
    });
  }

  function deleteQuestion(index) {
    updateQuestion((draft) => {
      draft.listOfQuestions.splice(index, 1);
    });
  }

  const postButton = async () => {
    const post = await axios
      .post(`https://quhaad.herokuapp.com/question/${props.roomId}`, question)
      .then((res) => {
        console.log(res.data);
        props.setQuestionSet(res.data);
      })
      .catch((error) => console.log(error));

    props.setIsHost(true);
    navigate("/game");
  };

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>{!initialInput ? "Enter Question Set Name" : "Question List"}</h1>
      <HorizontalSpacer />
      {!initialInput ? (
        <>
          <Input onChange={(e) => updateQuestionSetName(e.target.value)} />
          <HorizontalSpacer />
          <Button green onClick={redirectToCreateQuestion}>
            Next
          </Button>
        </>
      ) : (
        <>
          {detailQuestionPage ? (
            <CreatePage
              index={detailQuestionPage[1]}
              data={question.listOfQuestions[detailQuestionPage[1]]}
              updateQuestionText={updateQuestionText}
              updateMediaURL={updateMediaURL}
              updateTimeLimit={updateTimeLimit}
              updateAnswer1={updateAnswer1}
              updateAnswer2={updateAnswer2}
              updateAnswer3={updateAnswer3}
              updateAnswer4={updateAnswer4}
              updatePoint={updatePoint}
              updateCorrectAnswer={updateCorrectAnswer}
              backToQuestionList={backToQuestionList}
            />
          ) : (
            <>
              {question.listOfQuestions.map((value, index) => (
                <>
                  <YellowContainer>
                    <h2>{index + 1}.</h2>
                    <VerticalSpacer />
                    <WhiteContainer>{value.questionText}</WhiteContainer>
                    <img
                      src={edit}
                      onClick={() => redirectToDetailQuestion(index)}
                    />
                    <h1
                      style={{ cursor: "pointer" }}
                      onClick={() => deleteQuestion(index)}
                    >
                      X
                    </h1>
                  </YellowContainer>
                  <HorizontalSpacer />
                </>
              ))}
              <BottomContainer>
                <Button onClick={addQuestion}>Add More</Button>
                <HorizontalSpacer />
                <Button green onClick={postButton}>
                  Play
                </Button>
              </BottomContainer>
            </>
          )}
        </>
      )}
    </Container>
  );
};

export default QuestionList;

import React from "react";
import styled from "styled-components";

const StyledInput = styled.input`
  border-radius: 10px;
  height: 60px;
  padding: 0px 10px;
  font-family: "JejuHallasan";
  font-size: 30px;
`;

const Input = (props) => {
  return (
    <StyledInput
      onChange={props.onChange}
      placeholder={props.placeholder}
      id={props.id}
      {...props}
    />
  );
};

export default Input;

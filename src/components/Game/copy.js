import React, { useState, useRef, useEffect } from "react";
import GamePIN from "../GamePIN";
import NicknamePage from "../NicknamePage";
import ShowNickname from "../ShowNickname";
import GameOver from "../GameOver";
import FinalRank from "../FinalRank";
import ResultAnswer from "../ResultAnswer";
import Scoreboard from "../Scoreboard";
import ChooseAnswer from "../ChooseAnswer";
import ShowQuestion from "../ShowQuestion";
import ShowAnswer from "../ShowAnswer";
import Waiting from "../Waiting";
import AreYouReady from "../AreYouReady";
import GetReady from "../GetReady";
import webstomp from "webstomp-client";
import SockJS from "sockjs-client";
import axios from "axios";
import { hostMachine } from "../../utils/hostMachine";
import { useMachine } from "@xstate/react";

function Game(props) {
  const [gamePIN, setGamePin] = useState(false);
  const [nickname, setNickname] = useState("");
  const [click, setClick] = useState(false);
  const [question, setQuestion] = useState(0);
  const [showQuestion, setShowQuestion] = useState(false);
  const [waiting, setWaiting] = useState(false);
  const [showAnswer, setShowAnswer] = useState(false);
  const [wrong, setWrong] = useState(false);
  const [correct, setCorrect] = useState(false);
  const [pause, setPause] = useState(false);
  const [waitingPercentage, setWaitingPercentage] = useState(100);
  const [waitingPercentageAnswer, setWaitingPercentageAnswer] = useState(100);

  const [current, send] = useMachine(hostMachine);
  const { count } = current.context;

  let choices = {
    "1": "A",
    "2": "B",
    "3": "C",
    "4": "D",
  };

  let webSocket = useRef(null);
  let stompClient = useRef(null);

  const [startGame, setStartGame] = useState(false);

  const onChangeNickname = (e) => {
    setNickname(e.target.value);
  };

  function onMessageReceived(payload) {
    let message = JSON.parse(payload.body);
    let { content } = message;
    let { type } = message;
    if (type == "PLAY") {
      setStartGame(true);
    }
    if (type == "SHOW") {
      setShowQuestion(true);
    }
    if (type == "ANSWER") {
      setWaiting(false);
    }
    if (content == "ANSWERED") {
      setWaiting(false);
    }
    if (type == "FEEDBACK") {
      setWaiting(false);
    }
    if (type == "SUCCEED") {
      // setPlayerId(content);
    }
  }

  const play = () => {
    stompClient.current.send(
      "/app/message/all/room1",
      JSON.stringify({ type: "PLAY", content: "" }),
      {}
    );
    setStartGame(true);
  };

  const showQuestionToAll = () => {
    stompClient.current.send(
      "/app/message/show-question/room1",
      JSON.stringify({ type: "SHOW" }),
      {}
    );
    setShowQuestion(true);
  };

  const answerQuestion = (answer) => {
    stompClient.current.send(
      "/app/message/answer/room1",
      JSON.stringify({
        sender: nickname,
        content: answer,
        roomId: "room1",
        type: "ANSWER",
      }),
      {}
    );
    stompClient.current.send(
      "/app/message/host/room1",
      JSON.stringify({
        sender: nickname,
        content: answer,
        roomId: "room1",
        type: "ANSWER",
      }),
      {}
    );
    setShowQuestion(false);
    setWaiting(true);
    if (props.questionSet.listOfQuestions[question].correctAnswer == answer) {
      setCorrect(true);
    } else {
      setWrong(true);
    }
  };

  const sendAnswer = () => {
    setWaiting(false);
    setPause(true);
  };

  const submitUsername = async () => {
    if (nickname) {
      setClick(true);
      const getQuestion = await axios
        .get(`https://quhaad.herokuapp.com/question/${roomId}`)
        .then((res) => {
          console.log(res);
          props.setQuestionSet(res.data);
        })
        .catch((error) => console.log(error));

      webSocket.current = new SockJS("https://quhaad.herokuapp.com/ws");
      stompClient.current = webstomp.over(webSocket.current);
      stompClient.current.onreceive = onMessageReceived;
      stompClient.current.connect({}, () => {
        stompClient.current.subscribe("/topic/room/room1", onMessageReceived);
        stompClient.current.send(
          "/app/message/findRoom/room1",
          JSON.stringify({ type: "JOIN" }),
          {}
        );
        stompClient.current.send(
          "/app/message/username/room1",
          JSON.stringify({ sender: nickname, type: "JOIN" })
        );
      });
    }
  };

  // useEffect(() => {
  //   if (props.isHost) {
  //     webSocket.current = new SockJS("http://localhost:8080/ws");
  //     stompClient.current = webstomp.over(webSocket.current);

  //     function onMessageReceived(payload) {
  //       let message = JSON.parse(payload.body);
  //       let { content } = message;
  //       let { receiver } = message;
  //       if (content == "CREATED") {
  //       } else if (receiver) {
  //         props.changeReceiver(receiver);
  //       }
  //     }

  //     function onConnected() {
  //       stompClient.current.subscribe("/topic/room/" + "room1");
  //       var data = {};
  //       stompClient.current.send(
  //         "/app/message/createRoom/" + "room1",
  //         JSON.stringify(data),
  //         {}
  //       );
  //     }

  //     stompClient.current.onreceive = onMessageReceived;
  //     stompClient.current.connect({}, onConnected);
  //   }
  // }, []);

  useEffect(() => {}, [startGame]);

  return (
    <div {...props}>
      {/* <GamePIN /> */}
      {!click && !props.isHost && (
        <NicknamePage onClick={submitUsername} onChange={onChangeNickname} />
      )}
      {click && !props.isHost && !startGame && !showQuestion && (
        <ShowNickname nickname={nickname} />
      )}
      {props.receivers && props.isHost && !startGame && (
        <Waiting receivers={props.receivers} play={play} />
      )}
      {props.isHost && startGame && !showQuestion && (
        <AreYouReady
          question={question}
          setQuestion={setQuestion}
          setQuestionSet={props.setQuestionSet}
          questionSet={props.questionSet}
          setShowQuestion={showQuestionToAll}
        />
      )}
      {!props.isHost &&
        startGame &&
        !showQuestion &&
        !waiting &&
        !correct &&
        !wrong && <GetReady />}
      {props.isHost && startGame && showQuestion && waitingPercentage != 0 && (
        <ShowQuestion
          question={props.questionSet.listOfQuestions[question]}
          sendAnswer={sendAnswer}
          waitingPercentage={waitingPercentage}
          setWaitingPercentage={setWaitingPercentage}
        />
      )}
      {props.isHost && startGame && showQuestion && waitingPercentage == 0 && (
        <ShowAnswer
          answer={props.questionSet.listOfQuestions[question].correctAnswer}
        />
      )}
      {!props.isHost &&
        startGame &&
        showQuestion &&
        !waiting &&
        !correct &&
        !wrong && (
          <ChooseAnswer
            answerQuestion={answerQuestion}
            waitingPercentage={waitingPercentageAnswer}
            setWaitingPercentage={setWaitingPercentageAnswer}
            timeLimit={
              props.questionSet.listOfQuestions &&
              props.questionSet.listOfQuestions[question].timeLimit
            }
          />
        )}
      {!props.isHost && startGame && waitingPercentageAnswer != 0 && (
        <GetReady
          waiting={true}
          waitingPercentageAnswer={waitingPercentageAnswer}
          setWaitingPercentageAnswer={setWaitingPercentageAnswer}
          timeLimit={props.questionSet.listOfQuestions[question].timeLimit}
        />
      )}
      {!props.isHost &&
        startGame &&
        !waiting &&
        correct &&
        waitingPercentageAnswer == 0 && <ResultAnswer correct />}
      {!props.isHost &&
        startGame &&
        !waiting &&
        wrong &&
        waitingPercentageAnswer == 0 && <ResultAnswer />}
      {/* <GameOver />
      <FinalRank />
      <ResultAnswer />
      <Scoreboard /> */}
    </div>
  );
}

export default Game;

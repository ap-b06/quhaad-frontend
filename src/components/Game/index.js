import React, { useState, useRef, useEffect } from "react";
import GamePIN from "../GamePIN";
import NicknamePage from "../NicknamePage";
import ShowNickname from "../ShowNickname";
import GameOver from "../GameOver";
import FinalRank from "../FinalRank";
import ResultAnswer from "../ResultAnswer";
import Scoreboard from "../Scoreboard";
import ChooseAnswer from "../ChooseAnswer";
import ShowQuestion from "../ShowQuestion";
import ShowAnswer from "../ShowAnswer";
import Waiting from "../Waiting";
import AreYouReady from "../AreYouReady";
import GetReady from "../GetReady";
import webstomp from "webstomp-client";
import SockJS from "sockjs-client";
import axios from "axios";
import { hostMachine } from "../../utils/hostMachine";
import { playerMachine } from "../../utils/playerMachine";
import { useMachine } from "@xstate/react";
import Button from "../Button";
import Statistic from "../Statistic";

function Game(props) {
  const [current, send] = useMachine(hostMachine);
  const [playerState, setPlayerState] = useMachine(playerMachine);
  const { index, timer } = current.context;
  const { pointer, answerPointer } = playerState.context;
  const [nickname, setNickname] = useState("");
  const [click, setClick] = useState(false);
  const [question, setQuestion] = useState({});
  const [showQuestion, setShowQuestion] = useState(false);
  const [waiting, setWaiting] = useState(false);
  const [showAnswer, setShowAnswer] = useState(false);
  const [wrong, setWrong] = useState(false);
  const [correct, setCorrect] = useState(false);
  const [pause, setPause] = useState(false);
  const [waitingPercentage, setWaitingPercentage] = useState(timer);
  const [waitingPercentageAnswer, setWaitingPercentageAnswer] = useState(100);
  const [roomId, setRoomId] = useState("");
  const [nicknameError, setNicknameError] = useState("");
  const [startGame, setStartGame] = useState(false);
  const [answer, setAnswer] = useState("");

  let choices = {
    "1": "A",
    "2": "B",
    "3": "C",
    "4": "D",
  };

  let webSocket = useRef(null);
  let stompClient = useRef(null);

  const onChangeNickname = (e) => {
    setNickname(e.target.value);
  };

  const onChangePIN = (e) => {
    setRoomId(e.target.value);
  };

  function onMessageReceived(payload) {
    let message = JSON.parse(payload.body);
    let { content } = message;
    let { type } = message;
    let { receiver } = message;
    if (type == "PLAY") {
      if (!props.isHost && receiver == nickname) {
        setPlayerState("FETCH"); // dari idle ke ready
      }
    }
    if (type == "SHOW" && receiver == nickname) {
      setPlayerState("FETCH"); // dari ready ke play
    }
    if (type == "POSTED" && receiver == nickname) {
      setPlayerState("ANSWER");
    }
    if (type == "FEEDBACK" && receiver == nickname) {
      setPlayerState("FETCH"); // ke result
    }
    if (type == "SUCCEED" && receiver == nickname) {
      setPlayerState("FETCH");
    }
    if (content === "REINSERT") {
      setNicknameError("Insert different name");
    }
    if (content === "ROOM_FULL") {
      setNicknameError("Room is already full");
    }
  }

  const play = () => {
    stompClient.current.send(
      `/app/message/all/${props.roomId}`,
      JSON.stringify({ type: "PLAY", content: "" }),
      {}
    );
    send("FETCH");
  };

  const showQuestionToAll = () => {
    stompClient.current.send(
      `/app/message/show-question/${props.roomId}`,
      JSON.stringify({ type: "SHOW" }),
      {}
    );
    send("FETCH");
  };

  const answerQuestion = (answer) => {
    setAnswer(answer);
    stompClient.current.send(
      `/app/message/answer/${roomId}`,
      JSON.stringify({
        sender: nickname,
        content: answer,
        roomId: roomId,
        type: "ANSWER",
      }),
      {}
    );
    setPlayerState("ANSWER");
  };

  const getFeedback = (answer) => {
    stompClient.current.send(
      `/app/message/host/${roomId}`,
      JSON.stringify({
        sender: nickname,
        content: answer,
        roomId: roomId,
        type: "ANSWER",
      }),
      {}
    );
  };

  const triggerToScoreboard = () => {
    stompClient.current.send(
      `/app/message/all/${props.roomId}`,
      JSON.stringify({ type: "PLAY", content: "" }),
      {}
    );
    send("FETCH");
  };

  const triggerToStatistic = () => {
    stompClient.current.send(
      `/app/message/all/${props.roomId}`,
      JSON.stringify({ type: "PLAY", content: "" }),
      {}
    );
    send("FETCH");
  };

  const submitUsername = async () => {
    if (nickname) {
      const getQuestion = await axios
        .get(`https://quhaad.herokuapp.com/question/${roomId}`)
        .then((res) => {
          console.log(res);
          props.setQuestionSet(res.data);
        })
        .catch((error) => console.log(error));
      webSocket.current = new SockJS("https://quhaad.herokuapp.com/ws");
      stompClient.current = webstomp.over(webSocket.current);
      stompClient.current.onreceive = onMessageReceived;
      stompClient.current.connect({}, () => {
        stompClient.current.subscribe(
          `/topic/room/${roomId}`,
          onMessageReceived
        );
        stompClient.current.send(
          `/app/message/findRoom/${roomId}`,
          JSON.stringify({ type: "JOIN" }),
          {}
        );
        stompClient.current.send(
          `/app/message/username/${roomId}`,
          JSON.stringify({ sender: nickname, type: "JOIN" })
        );
      });
    }
  };

  useEffect(() => {
    setWaitingPercentageAnswer(100);
  }, [pointer]);

  useEffect(() => {
    setAnswer("");
  }, [answerPointer]);

  useEffect(() => {
    if (props.isHost) {
      webSocket.current = new SockJS("https://quhaad.herokuapp.com/ws");
      stompClient.current = webstomp.over(webSocket.current);

      function onMessageReceived(payload) {
        let message = JSON.parse(payload.body);
        let { content } = message;
        let { receiver } = message;
        if (content == "CREATED") {
        } else if (
          receiver &&
          content != "ROOM_FULL" &&
          content != "REINSERT"
        ) {
          props.changeReceiver(receiver);
        }
      }

      function onConnected() {
        stompClient.current.subscribe("/topic/room/" + props.roomId);
        var data = {};
        stompClient.current.send(
          "/app/message/createRoom/" + props.roomId,
          JSON.stringify(data),
          {}
        );
      }

      stompClient.current.onreceive = onMessageReceived;
      stompClient.current.connect({}, onConnected);
    }
  }, []);

  const CheckBackToPlayOrGameOver = async () => {
    setWaitingPercentage(100);
    if (props.questionSet.listOfQuestions.length - 1 == index) {
      send("END");
    } else {
      const nextRound = await axios
        .get(`https://quhaad.herokuapp.com/next-round/${props.roomId}`)
        .then((res) => {
          console.log(res);
        })
        .catch((error) => console.log(error));
      send("BACK_TO_PLAY");
    }
  };

  return (
    <div {...props}>
      {props.isHost && current.value === "idle" && (
        <Waiting
          receivers={props.receivers}
          play={play}
          roomId={props.roomId}
        />
      )}
      {props.isHost && current.value === "ready" && (
        <AreYouReady
          question={question}
          setQuestion={setQuestion}
          setQuestionSet={props.setQuestionSet}
          questionSet={props.questionSet}
          setShowQuestion={showQuestionToAll}
        />
      )}
      {props.isHost && current.value === "play" && (
        <ShowQuestion
          question={props.questionSet.listOfQuestions[index]}
          waitingPercentage={waitingPercentage}
          setWaitingPercentage={setWaitingPercentage}
          send={send}
        />
      )}
      {props.isHost && current.value === "result" && (
        <ShowAnswer
          answer={props.questionSet.listOfQuestions[index].correctAnswer}
          showStatistic={triggerToStatistic}
        />
      )}
      {props.isHost && current.value === "statistic" && (
        <>
          <Statistic onClick={triggerToScoreboard} roomId={props.roomId} />
        </>
      )}
      {props.isHost && current.value === "scoreboard" && (
        <>
          <Scoreboard
            onClick={CheckBackToPlayOrGameOver}
            roomId={props.roomId}
          />
        </>
      )}
      {props.isHost && current.value === "gameover" && (
        <FinalRank roomId={props.roomId} />
      )}
      {!props.isHost && playerState.value === "findRoom" && (
        <GamePIN
          inputPIN={roomId}
          onChange={onChangePIN}
          setPlayerState={setPlayerState}
        />
      )}
      {!props.isHost && playerState.value === "nickname" && (
        <NicknamePage
          onClick={submitUsername}
          onChange={onChangeNickname}
          error={nicknameError}
        />
      )}
      {!props.isHost && playerState.value === "idle" && (
        <>
          <ShowNickname nickname={nickname} />
        </>
      )}
      {!props.isHost && playerState.value === "ready" && (
        <>
          <GetReady
            waiting={false}
            waitingPercentageAnswer={waitingPercentageAnswer}
            setWaitingPercentageAnswer={setWaitingPercentageAnswer}
            timeLimit={props.questionSet.listOfQuestions[index].timeLimit}
          />
        </>
      )}
      {!props.isHost && playerState.value === "play" && (
        <>
          <ChooseAnswer
            answer={answer}
            answerQuestion={answerQuestion}
            waitingPercentage={waitingPercentageAnswer}
            setWaitingPercentage={setWaitingPercentageAnswer}
            timeLimit={
              props.questionSet.listOfQuestions &&
              props.questionSet.listOfQuestions[index].timeLimit
            }
            getFeedback={getFeedback}
          />
        </>
      )}
      {!props.isHost && playerState.value === "waiting" && (
        <>
          <GetReady
            waiting={true}
            waitingPercentageAnswer={waitingPercentageAnswer}
            setWaitingPercentageAnswer={setWaitingPercentageAnswer}
            timeLimit={props.questionSet.listOfQuestions[index].timeLimit}
            answer={answer}
            getFeedback={getFeedback}
          />
        </>
      )}
      {!props.isHost &&
        (playerState.value === "result" ||
          playerState.value === "scoreboard" ||
          playerState.value === "statistic") && (
          <>
            <ResultAnswer nickname={nickname} roomId={roomId} />{" "}
          </>
        )}
    </div>
  );
}

export default Game;

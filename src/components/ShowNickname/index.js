import React, { useState } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import Input from "../Input";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;

  h4 {
    color: ${(props) => props.theme.colors.yellow};
  }
`;

const ShowNickname = (props) => {
  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>Welcome {props.nickname}</h1>
      <HorizontalSpacer />
      <h4>See your name on the screen?</h4>
    </Container>
  );
};

export default ShowNickname;

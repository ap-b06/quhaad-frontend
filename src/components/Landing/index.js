import React, { useState } from "react";
import { Link } from "@reach/router";
import styled from "styled-components";
import LandingCard from "../LandingCard";
import quiz from "../../static/quiz.png";
import how from "../../static/how.png";
import play from "../../static/play.png";
import mobileQuiz from "../../static/mobile-quiz.png";
import mobileHow from "../../static/mobile-how.png";
import mobilePlay from "../../static/mobile-play.png";
import { userAgentUtils } from "../../utils/userAgentUtils";
import VerticalSpacer from "../VerticalSpacer";
import HorizontalSpacer from "../HorizontalSpacer";

const Container = styled.div`
  text-align: center;

  a {
    text-decoration: none;
  }
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Landing = () => {
  const platform = userAgentUtils();

  return (
    <Container>
      {platform == "Web" ? (
        <Row>
          <LandingCard text={"What is Quhaad?"} image={quiz} red />
          <VerticalSpacer />
          <LandingCard text={"How to Play?"} image={how} yellow />
          <VerticalSpacer />
          <Link to="/create-play">
            <LandingCard text={"Let's Play!"} image={play} />
          </Link>
        </Row>
      ) : (
        <Column>
          <LandingCard text={"What is Quhaad?"} image={mobileQuiz} red />
          <HorizontalSpacer />
          <LandingCard text={"How to Play?"} image={mobileHow} yellow />
          <HorizontalSpacer />
          <Link to="/create-play">
            <LandingCard text={"Let's Play!"} image={mobilePlay} />
          </Link>
        </Column>
      )}
    </Container>
  );
};

export default Landing;

import styled from "styled-components";

const VerticalSpacer = styled.div`
  width: 40px;
`;

export default VerticalSpacer;

import React, { useState } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import Input from "../Input";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
`;

const ShowAnswer = (props) => {
  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>Jawaban Benar: {props.answer}</h1>
      <Button onClick={props.showStatistic}>Next</Button>
      <HorizontalSpacer />
    </Container>
  );
};

export default ShowAnswer;

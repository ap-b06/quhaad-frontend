import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import { Bar, BarChart, LabelList, XAxis, Cell } from "recharts";
import axios from "axios";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;

  text {
    fill: white !important;
  }
`;

const Statistic = (props) => {
  const [scoreboardData, setScoreboardData] = useState([]);

  const getData = async () => {
    const getFromAxios = await axios
      .get(`https://quhaad.herokuapp.com/scoreboard/${props.roomId}`, {})
      .then((res) => {
        console.log(res.data.pq);
        setScoreboardData(res.data.pq.reverse().slice(0, 3));
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getData();
  }, []);

  let data;

  if (scoreboardData.length === 1) {
    data = [
      {
        name: scoreboardData.length > 0 && scoreboardData[0].username,
        value: 12,
      },
    ];
  } else if (scoreboardData.length === 2) {
    data = [
      {
        name: scoreboardData.length > 0 && scoreboardData[1].username,
        value: 7,
      },
      {
        name: scoreboardData.length > 0 && scoreboardData[0].username,
        value: 12,
      },
    ];
  } else if (scoreboardData.length === 3) {
    data = [
      {
        name: scoreboardData.length > 0 && scoreboardData[1].username,
        value: 7,
      },
      {
        name: scoreboardData.length > 0 && scoreboardData[0].username,
        value: 12,
      },
      {
        name: scoreboardData.length > 0 && scoreboardData[2].username,
        value: 4,
      },
    ];
  }

  const colors = ["#E44848", "#F3C213", "#3BDD21", "#2EC1CA"];

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>And the winner is...</h1>
      <BarChart isAnimationActive={false} width={730} height={250} data={data}>
        {scoreboardData.length > 0 && (
          <Bar dataKey="value">
            <Cell key={`cell-0`} fill={colors[0]} />
            <Cell key={`cell-1`} fill={colors[1]} />
            <Cell key={`cell-2`} fill={colors[2]} />
            <LabelList dataKey="name" position="inside" />
          </Bar>
        )}
      </BarChart>

      <HorizontalSpacer />
    </Container>
  );
};

export default Statistic;

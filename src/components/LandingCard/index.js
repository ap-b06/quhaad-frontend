import React from "react";
import styled from "styled-components";
import { motion } from "framer-motion";

const Container = styled(motion.div)`
  border-radius: 46px;
  background: ${props =>
    props.red
      ? "linear-gradient(180deg, #E12B2B 0%, rgba(231, 37, 37, 0.666667) 51.56%, rgba(243, 24, 24, 0) 100%)"
      : props.yellow
      ? "linear-gradient(180deg, #F3C213 0%, rgba(214, 171, 17, 0.598958) 65.1%, rgba(171, 137, 14, 0) 100%);"
      : "linear-gradient(180deg, #3FF521 0%, rgba(72, 200, 26, 0.578125) 58.85%, rgba(85, 137, 18, 0) 100%)"};
  display: flex;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  color: ${props => props.theme.colors.white};
  max-width: 350px;
  height: 100%;

  img {
    width: 100%;
    opacity: 0.45;
    border-radius: 22px;
  }

  @media (max-width: 768px) {
    max-width: 250px;
  }
`;

const LandingCard = props => {
  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
      whileHover={{ scale: 1.1 }}
      {...props}
    >
      <h1>{props.text}</h1>
      <img src={props.image} alt={props.alt} />
    </Container>
  );
};

export default LandingCard;

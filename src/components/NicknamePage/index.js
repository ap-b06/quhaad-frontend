import React from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import Input from "../Input";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
`;

const NicknamePage = (props) => {
  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>Choose your Nickname</h1>
      <HorizontalSpacer />
      <Input onChange={props.onChange} />
      <HorizontalSpacer />
      {props.error && <p style={{ color: "#E44848" }}>{props.error}</p>}
      <Button onClick={props.onClick}>Join</Button>
    </Container>
  );
};

export default NicknamePage;

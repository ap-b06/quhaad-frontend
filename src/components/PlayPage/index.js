import React from "react";
import styled from "styled-components";
import Button from "../Button";
import VerticalSpacer from "../VerticalSpacer";
import { motion } from "framer-motion";
import { Link } from "@reach/router";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Text = styled.h1`
  color: ${(props) => props.theme.colors.yellow};
`;

const PlayPage = () => {
  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>Let's Go!</h1>
      <Row>
        <Link to="/room-id">
          <Button red>
            <h1>Create</h1>
          </Button>
        </Link>
        <VerticalSpacer />
        <Text>or</Text>
        <VerticalSpacer />
        <Link to="/game">
          <Button green>
            <h1>Play</h1>
          </Button>
        </Link>
      </Row>
    </Container>
  );
};

export default PlayPage;

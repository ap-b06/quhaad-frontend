import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import axios from "axios";

const ScoreContainer = styled.div`
  border-radius: 30px;
  width: 100%;
  background-color: ${(props) => props.theme.colors.yellow};
  padding-left: 50px;
  padding-right: 50px;
  padding-top: 20px;
  padding-bottom: 20px;
`;

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  padding-left: 100px;
  padding-right: 100px;
`;

const RankList = styled.div`
  display: flex;
  justify-content: space-between;
  background-color: ${(props) => props.theme.colors.white};
  color: ${(props) => props.theme.colors.headBlue};
  border-radius: 30px;
  padding-left: 40px;
  padding-right: 40px;
  margin-bottom: 10px;
`;

const Scoreboard = (props) => {
  const [scoreboardData, setScoreboardData] = useState([]);

  const getData = async () => {
    const getFromAxios = await axios
      .get(`https://quhaad.herokuapp.com/scoreboard/${props.roomId}`, {})
      .then((res) => {
        console.log(res.data.pq);
        setScoreboardData(res.data.pq.reverse());
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
      {...props}
    >
      <h1>Scoreboard</h1>
      <ScoreContainer>
        {scoreboardData.map((value, index) => (
          <RankList key={index}>
            <h4>{value.username}</h4>
            <h4>{value.score}</h4>
          </RankList>
        ))}
      </ScoreContainer>
      <HorizontalSpacer />
      <Button onClick={props.onClick}>Next</Button>
    </Container>
  );
};

export default Scoreboard;

import React, { useState, useEffect, useRef } from "react";
import useInterval from "../../utils/useInterval";
import styled from "styled-components";
import Button from "../Button";
import { motion } from "framer-motion";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;

  h1 {
    color: ${(props) => props.theme.colors.white};
  }
`;

const ProgressBar = (props) => (
  <div
    style={{
      display: "block",
      width: `${props.percentage}%`,
      height: 20,
      backgroundColor: "#F3C213",
    }}
  />
);

const AreYouReady = (props) => {
  const [percentage, setPercentage] = useState(100);

  useEffect(() => {
    const timeout = setTimeout(() => {
      let nextPercent = percentage - 1;
      if (nextPercent < 0) {
        nextPercent = 0;
        props.setShowQuestion();
      }
      setPercentage(nextPercent);
    }, 20);
    return () => clearTimeout(timeout);
  }, [percentage]);

  return (
    <>
      <Container
        initial={{ translateY: 1000, opacity: 0 }}
        animate={{ translateY: 0, opacity: 1 }}
      >
        <h1>Are You Ready ?</h1>
        <Button>
          There are {props.questionSet.listOfQuestions.length} questions ...
        </Button>
      </Container>
      <div style={{ padding: "75px" }}>
        <ProgressBar percentage={percentage} />
      </div>
    </>
  );
};

export default AreYouReady;

import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;

  h4 {
    color: ${(props) => props.theme.colors.yellow};
  }
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
`;

const ButtonA = styled.div`
  border-radius: 30px;
  background-color: ${(props) => props.theme.colors.red};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  width: 200px;
  height: 200px;
  margin-right: 20px;
`;

const ButtonB = styled.div`
  border-radius: 30px;
  background-color: ${(props) => props.theme.colors.yellow};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  width: 200px;
  height: 200px;
  margin-right: 20px;
`;

const ButtonC = styled.div`
  border-radius: 30px;
  background-color: ${(props) => props.theme.colors.green};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  width: 200px;
  height: 200px;
  margin-right: 20px;
`;

const ButtonD = styled.div`
  border-radius: 30px;
  background-color: ${(props) => props.theme.colors.tortoise};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  width: 200px;
  height: 200px;
`;

const ChooseAnswer = (props) => {
  useEffect(() => {
    const timeout = setTimeout(() => {
      let nextPercent = props.waitingPercentage - 1;
      if (nextPercent < 0) {
        nextPercent = 0;
        if (props.answer === "") {
          props.getFeedback("");
        }
      }
      props.setWaitingPercentage(nextPercent);
    }, props.timeLimit * 10);
    return () => clearTimeout(timeout);
  }, [props.waitingPercentage]);

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
      {...props}
    >
      <h1>Choose your answer!</h1>
      <HorizontalSpacer />
      <Row>
        <ButtonA onClick={() => props.answerQuestion("A")}>
          <h1>A</h1>
        </ButtonA>
        <ButtonB onClick={() => props.answerQuestion("B")}>
          <h1>B</h1>
        </ButtonB>
        <ButtonC onClick={() => props.answerQuestion("C")}>
          <h1>C</h1>
        </ButtonC>
        <ButtonD onClick={() => props.answerQuestion("D")}>
          <h1>D</h1>
        </ButtonD>
      </Row>
    </Container>
  );
};

export default ChooseAnswer;

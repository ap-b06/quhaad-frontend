import React, { useState } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import Input from "../Input";
import axios from "axios";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
`;

const GamePIN = (props) => {
  const [error, setError] = useState("");

  const checkRoomId = async () => {
    if (props.inputPIN != "") {
      const post = await axios
        .get(`https://quhaad.herokuapp.com/check/${props.inputPIN}`, {})
        .then((res) => {
          console.log(res.data);
          if (res.data === "ROOM DOES NOT EXIST") {
            setError("Room does not exist");
          } else {
            props.setPlayerState("FETCH");
          }
        })
        .catch((error) => console.log(error));
    } else {
      setError("Room ID cannot be blank");
    }
  };

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>Enter the Game PIN</h1>
      <HorizontalSpacer />
      <Input onChange={props.onChange} />
      <HorizontalSpacer />
      {error && <p style={{ color: "#E44848" }}>{error}</p>}
      <Button onClick={checkRoomId}>Join</Button>
    </Container>
  );
};

export default GamePIN;

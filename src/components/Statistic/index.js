import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import { Bar, BarChart, LabelList, XAxis, Cell } from "recharts";
import axios from "axios";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;

  text {
    fill: white !important;
  }
`;

const Statistic = (props) => {
  const [statisticData, setStatisticData] = useState({});

  const getData = async () => {
    const getFromAxios = await axios
      .get(`https://quhaad.herokuapp.com/statistic/${props.roomId}`, {})
      .then((res) => {
        console.log(res.data);
        setStatisticData(res.data);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getData();
  }, []);

  const data = [
    { name: "A", value: statisticData.answerACount },
    { name: "B", value: statisticData.answerBCount },
    { name: "C", value: statisticData.answerCCount },
    { name: "D", value: statisticData.answerDCount },
  ];

  const colors = ["#E44848", "#F3C213", "#3BDD21", "#2EC1CA"];

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
    >
      <h1>Statistic</h1>
      <BarChart isAnimationActive={false} width={730} height={250} data={data}>
        <XAxis dataKey="name" />
        <Bar dataKey="value">
          <Cell key={`cell-1`} fill={colors[0]} />
          <Cell key={`cell-2`} fill={colors[1]} />
          <Cell key={`cell-3`} fill={colors[2]} />
          <Cell key={`cell-4`} fill={colors[3]} />
          <LabelList dataKey="value" position="inside" />
        </Bar>
      </BarChart>
      <HorizontalSpacer />
      <Button onClick={props.onClick}>Next</Button>
    </Container>
  );
};

export default Statistic;

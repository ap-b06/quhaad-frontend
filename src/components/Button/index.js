import React from "react";
import styled from "styled-components";
import { motion } from "framer-motion";

const Container = styled(motion.button)`
  padding: 10px 84px;
  background-color: ${(props) =>
    props.red
      ? props.theme.colors.red
      : props.green
      ? props.theme.colors.green
      : props.theme.colors.yellow};
  border-radius: 40px;
  color: #ffffff;
  border: none;
`;

const Button = (props) => {
  return (
    <Container {...props} whileHover={{ scale: 1.1 }}>
      <h1>{props.children}</h1>
    </Container>
  );
};

export default Button;

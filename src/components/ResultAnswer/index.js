import React, { useState, useEffect } from "react";
import styled from "styled-components";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";
import axios from "axios";

const AnswerContainer = styled.div`
  border-radius: 30px;
  width: 100%;
`;

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  padding-left: 50px;
  padding-right: 50px;

  h4 {
    color: ${(props) =>
      props.correct ? props.theme.colors.white : props.theme.colors.yellow};
  }

  ${AnswerContainer} {
    background-color: ${(props) =>
      props.correct ? props.theme.colors.green : props.theme.colors.red};
  }
`;

const ResultAnswer = (props) => {
  const [resultData, setResultData] = useState([]);
  const [rank, setRank] = useState(0);

  const getData = async () => {
    const getFromAxios = await axios
      .get(`https://quhaad.herokuapp.com/scoreboard/${props.roomId}`, {})
      .then((res) => {
        console.log(res.data.pq);
        let data = res.data.pq.reverse();
        data.find((value) => {
          if (value.username == props.nickname) {
            setResultData(value);
          }
        });

        setRank(
          data.findIndex((value) => value.username === props.nickname) + 1
        );
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
      {...props}
    >
      {resultData.answer == "CORRECT" ? (
        <AnswerContainer style={{ background: "#3BDD21" }}>
          <h1>Horray! You're correct</h1>
          <h1>V</h1>
          <h4 style={{ color: "#fff" }}>You have {resultData.score} points</h4>
          <h4 style={{ color: "#fff" }}>You're now in {rank} place</h4>
        </AnswerContainer>
      ) : (
        <AnswerContainer>
          <h1>Oh No.. You're wrong</h1>
          <h1>X</h1>
          <h4>You're now in {rank} place</h4>
        </AnswerContainer>
      )}
    </Container>
  );
};

export default ResultAnswer;

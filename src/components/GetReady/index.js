import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { motion } from "framer-motion";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;

  h1 {
    color: ${(props) => props.theme.colors.white};
  }

  .loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid ${(props) => props.theme.colors.yellow}; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
  }

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

const GetReady = (props) => {
  useEffect(() => {
    if (props.timeLimit) {
      const timeout = setTimeout(() => {
        let nextPercent = props.waitingPercentageAnswer - 1;
        if (nextPercent < 0) {
          nextPercent = 0;
          if (props.answer !== "" && typeof props.getFeedback !== "undefined") {
            props.getFeedback(props.answer);
          }
        }
        props.setWaitingPercentageAnswer(nextPercent);
      }, props.timeLimit * 10);
      return () => clearTimeout(timeout);
    }
  }, [props.waitingPercentageAnswer]);

  return (
    <Container
      initial={{ translateY: 1000, opacity: 0 }}
      animate={{ translateY: 0, opacity: 1 }}
      {...props}
    >
      <h1>{props.waiting ? "Waiting" : "Get Ready"}</h1>
      <div class="loader"></div>
    </Container>
  );
};

export default GetReady;

import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Button from "../Button";
import HorizontalSpacer from "../HorizontalSpacer";
import { motion } from "framer-motion";

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  padding-left: 200px;
  padding-right: 200px;

  h4 {
    color: ${(props) => props.theme.colors.yellow};
  }
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${(props) => props.theme.colors.grey};
  border-radius: 30px;
  padding: 10px;
  margin-right: 50px;
  margin-left: 50px;
  width: 100%;
  align-items: center;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  margin-bottom: 10px;
`;

const ButtonA = styled.div`
  border-radius: 30px;
  background-color: ${(props) => props.theme.colors.red};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  margin-right: 20px;
  width: 50%;
`;

const ButtonB = styled.div`
  border-radius: 30px;
  background-color: ${(props) => props.theme.colors.yellow};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  width: 50%;
`;

const ButtonC = styled.div`
  border-radius: 30px;
  background-color: ${(props) => props.theme.colors.green};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  margin-right: 20px;
  width: 50%;
`;

const ButtonD = styled.div`
  border-radius: 30px;
  background-color: ${(props) => props.theme.colors.tortoise};
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
  width: 50%;
`;

const ProgressBar = (props) => (
  <div
    style={{
      display: "block",
      width: `${props.percentage}%`,
      height: 20,
      backgroundColor: "#F3C213",
    }}
  />
);

const ShowQuestion = (props) => {
  let nextPercent;
  if (props.waitingPercentage == 0) {
    props.send("FETCH");
  }

  useEffect(() => {
    const timeout = setTimeout(() => {
      let nextPercent = props.waitingPercentage - 1;
      if (nextPercent < 0) {
        nextPercent = 0;
      }
      props.setWaitingPercentage(nextPercent);
    }, props.question.timeLimit * 10);
    return () => clearTimeout(timeout);
  }, [props.waitingPercentage]);

  return (
    <>
      <Container
        initial={{ translateY: 1000, opacity: 0 }}
        animate={{ translateY: 0, opacity: 1 }}
        {...props}
      >
        {props.question.questionMediaUrl != null && (
          <img src={props.question.questionMediaUrl} />
        )}
        <h1>{props.question.questionText}</h1>
        <HorizontalSpacer />
        <Column>
          <Row>
            <ButtonA>
              <h1>{props.question.answer1}</h1>
            </ButtonA>
            <ButtonB>
              <h1>{props.question.answer2}</h1>
            </ButtonB>
          </Row>
          <Row>
            <ButtonC>
              <h1>{props.question.answer3}</h1>
            </ButtonC>
            <ButtonD>
              <h1>{props.question.answer4}</h1>
            </ButtonD>
          </Row>
        </Column>
      </Container>
      <div style={{ padding: "75px" }}>
        <ProgressBar percentage={props.waitingPercentage} />
      </div>
    </>
  );
};

export default ShowQuestion;

import React, { useState } from "react";
import styled from "styled-components";
import Input from "../Input";
import Button from "../Button";
import CorrectButton from "../CorrectButton";
import { motion } from "framer-motion";
import VerticalSpacer from "../VerticalSpacer";

const Container = styled(motion.div)``;

const Text = styled.h1`
  color: ${(props) => props.theme.colors.yellow};
`;

const LowerContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const AnswerBoxRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const CreatePage = (props) => {
  const [questionTextError, setQuestionTextError] = useState(false);
  const [correctAnswerError, setCorrectAnswerError] = useState(false);
  const [timeLimitError, setTimeLimitError] = useState(false);
  const [pointError, setPointError] = useState(false);
  const [answer1Error, setAnswer1Error] = useState(false);
  const [answer2Error, setAnswer2Error] = useState(false);
  const [answer3Error, setAnswer3Error] = useState(false);
  const [answer4Error, setAnswer4Error] = useState(false);

  const checkValidationForm = () => {
    setQuestionTextError(false);
    setCorrectAnswerError(false);
    setTimeLimitError(false);
    setPointError(false);
    setAnswer1Error(false);
    setAnswer2Error(false);
    setAnswer3Error(false);
    setAnswer4Error(false);
    if (props.data.questionText === "") {
      setQuestionTextError(true);
    }
    if (props.data.correctAnswer === "") {
      setCorrectAnswerError(true);
    }
    if (isNaN(props.data.timeLimit)) {
      setTimeLimitError(true);
    }
    if (isNaN(props.data.point)) {
      setPointError(true);
    }
    if (props.data.answer1 === null) {
      setAnswer1Error(true);
    }
    if (props.data.answer2 === null) {
      setAnswer2Error(true);
    }
    if (props.data.answer3 === null) {
      setAnswer3Error(true);
    }
    if (props.data.answer4 === null) {
      setAnswer4Error(true);
    }
  };

  function redirectToQuestionList() {
    if (
      props.data.questionText !== "" &&
      props.data.correctAnswer !== "" &&
      !isNaN(props.data.timeLimit) &&
      !isNaN(props.data.point) &&
      props.data.answer1 !== null &&
      props.data.answer2 !== null &&
      props.data.answer3 !== null &&
      props.data.answer4 !== null
    ) {
      props.backToQuestionList();
    }
  }

  function checkAndRedirect() {
    checkValidationForm();
    redirectToQuestionList();
  }

  return (
    <Container {...props}>
      <LowerContainer>
        <AnswerBoxRow>
          <div>
            <Text>Question</Text>
            <AnswerBoxRow>
              <Input
                placeholder="Add your question"
                defaultValue={props.data.questionText}
                onChange={(e) =>
                  props.updateQuestionText(e.target.value, props.index)
                }
              />
              <VerticalSpacer />
            </AnswerBoxRow>
            {questionTextError && (
              <p style={{ color: "#E44848" }}>This field cannot be blank.</p>
            )}
          </div>
          <VerticalSpacer />
          <div>
            <Text>Media</Text>
            <AnswerBoxRow>
              <Input
                placeholder="Add image URL"
                defaultValue={props.data.mediaUrl}
                onChange={(e) =>
                  props.updateMediaURL(e.target.value, props.index)
                }
              />
              <VerticalSpacer />
            </AnswerBoxRow>
            {questionTextError && (
              <p style={{ visibility: "hidden" }}>This is only for styling</p>
            )}
          </div>
        </AnswerBoxRow>
        <AnswerBoxRow>
          <div>
            <Text>Time Limit</Text>
            <AnswerBoxRow>
              <Input
                type="number"
                min="1"
                defaultValue={props.data.timeLimit}
                placeholder="Add time limit"
                onChange={(e) =>
                  props.updateTimeLimit(e.target.value, props.index)
                }
              />
              <VerticalSpacer />
            </AnswerBoxRow>
            {timeLimitError && (
              <p style={{ color: "#E44848" }}>This field cannot be blank.</p>
            )}
          </div>
          <div>
            <Text>Point</Text>
            <AnswerBoxRow>
              <Input
                min="1"
                defaultValue={props.data.point}
                placeholder="Add point"
                onChange={(e) => props.updatePoint(e.target.value, props.index)}
              />
              <VerticalSpacer />
            </AnswerBoxRow>
            {pointError && (
              <p style={{ color: "#E44848" }}>This field cannot be blank.</p>
            )}
          </div>
        </AnswerBoxRow>
        <AnswerBoxRow>
          <div>
            <Text>Answer 1</Text>
            <AnswerBoxRow>
              <Input
                placeholder="Add answer 1"
                defaultValue={props.data.answer1}
                onChange={(e) =>
                  props.updateAnswer1(e.target.value, props.index)
                }
              />
              <VerticalSpacer />
              <CorrectButton
                correct={props.data.correctAnswer === "A"}
                onClick={() => props.updateCorrectAnswer("A", props.index)}
              />
            </AnswerBoxRow>
            {answer1Error && (
              <p style={{ color: "#E44848" }}>This field cannot be blank.</p>
            )}
          </div>
          <VerticalSpacer />
          <div>
            <Text>Answer 3</Text>
            <AnswerBoxRow>
              <Input
                placeholder="Add answer 3"
                defaultValue={props.data.answer3}
                onChange={(e) =>
                  props.updateAnswer3(e.target.value, props.index)
                }
              />
              <VerticalSpacer />
              <CorrectButton
                correct={props.data.correctAnswer === "C"}
                onClick={() => props.updateCorrectAnswer("C", props.index)}
              />
            </AnswerBoxRow>
            {answer3Error && (
              <p style={{ color: "#E44848" }}>This field cannot be blank.</p>
            )}
          </div>
        </AnswerBoxRow>
        <AnswerBoxRow>
          <div>
            <Text>Answer 2</Text>
            <AnswerBoxRow>
              <Input
                placeholder="Add answer 2"
                defaultValue={props.data.answer2}
                onChange={(e) =>
                  props.updateAnswer2(e.target.value, props.index)
                }
              />
              <VerticalSpacer />
              <CorrectButton
                correct={props.data.correctAnswer === "B"}
                onClick={() => props.updateCorrectAnswer("B", props.index)}
              />
            </AnswerBoxRow>
            {answer2Error && (
              <p style={{ color: "#E44848" }}>This field cannot be blank.</p>
            )}
          </div>
          <VerticalSpacer />
          <div>
            <Text>Answer 4</Text>
            <AnswerBoxRow>
              <Input
                placeholder="Add answer 4"
                defaultValue={props.data.answer4}
                onChange={(e) =>
                  props.updateAnswer4(e.target.value, props.index)
                }
              />
              <VerticalSpacer />
              <CorrectButton
                correct={props.data.correctAnswer === "D"}
                onClick={() => props.updateCorrectAnswer("D", props.index)}
              />
            </AnswerBoxRow>
            {answer4Error && (
              <p style={{ color: "#E44848" }}>This field cannot be blank.</p>
            )}
          </div>
        </AnswerBoxRow>
      </LowerContainer>
      <AnswerBoxRow
        style={{
          alignItems: "center",
          justifyContent: "center",
          marginTop: "40px",
        }}
      >
        <LowerContainer>
          {correctAnswerError && (
            <p style={{ color: "#E44848" }}>
              You haven't select correct answer.
            </p>
          )}
          <Button green onClick={() => checkAndRedirect()}>
            Save
          </Button>
        </LowerContainer>
      </AnswerBoxRow>
    </Container>
  );
};

export default CreatePage;

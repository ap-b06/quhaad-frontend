let backend, ws;

const hostname = window.location.origin;

if (hostname === "http://localhost:3000/") {
  backend = "http://localhost:8080/";
  ws = "http://localhost:8080/ws";
}

export const API_ROOT = `${backend}`;
export const WS = `${ws}`;
